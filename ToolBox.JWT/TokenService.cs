﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;



namespace ToolBox.JWT
{
    public class TokenService
    {
        private JwtSecurityTokenHandler _handler;
        private string _signature;
        private string _audience;
        private string _issuer;

        public TokenService()
        {
            _handler = new JwtSecurityTokenHandler();
            _signature = System.Configuration.ConfigurationManager.AppSettings.Get("signature");
            _issuer = System.Configuration.ConfigurationManager.AppSettings.Get("issuer");
            _audience = System.Configuration.ConfigurationManager.AppSettings.Get("audience");
        }

        public string ConfigurationManager { get; }

        public string Encode<T>(T o)
        {
            JwtSecurityToken token 
                = new JwtSecurityToken(
                    _issuer,
                    _audience,
                    GetClaims<T>(o),
                    DateTime.UtcNow,
                    DateTime.UtcNow.AddDays(1),
                    GetCredentials()
                    );

            return _handler.WriteToken(token);
        }


        public ClaimsPrincipal Decode(string sToken)
        {
            try
            {
                ClaimsPrincipal result = _handler.ValidateToken(
                sToken, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    IssuerSigningKey
                        = new SymmetricSecurityKey(
                                Encoding.UTF8.GetBytes(_signature)),
                    ValidIssuer = _issuer,
                    ValidAudience = _audience,
                    //Supprimer la tolérence de 5 minutes pour la validité du token
                    //ClockSkew = TimeSpan.Zero
                },
                out SecurityToken t
                ); ;
                return result;
            }
            catch (Exception e) { return null; }
        }


        private SigningCredentials GetCredentials()
        {
            return new SigningCredentials( 
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_signature)) , 
                SecurityAlgorithms.HmacSha512);
        }

        private IEnumerable<Claim> GetClaims<T>(T o)
        {
            foreach(PropertyInfo p in typeof(T).GetProperties())
            {
                if(p.GetValue(o) != null)
                yield return new Claim(p.Name, p.GetValue(o).ToString());
            }
        }


    }
}
