using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using api_heartreate.Repository;
using api_heartreate.Services;
using capteur.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace api_heartreate
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder => builder
                .SetIsOriginAllowed(_ => true)
                .AllowAnyMethod()
                .AllowAnyHeader()
                //.AllowAnyOrigin()
                .AllowCredentials());
        });


            services.AddControllers();

            services.AddScoped<EntryService>();
            services.AddScoped<UserService>();
            services.AddScoped<SessionsServices>();
            services.AddSignalR();


            string connString = Configuration.GetSection("Connection").GetSection("ConnectionString").Value;
            string providerName = Configuration.GetSection("Connection").GetSection("ProviderName").Value;
            services.AddScoped<EntryRepository>((s) => new EntryRepository(connString, providerName));
            services.AddScoped<UserRepository>((s) => new UserRepository(connString, providerName));
            services.AddScoped<SessionsRepository>((s) => new SessionsRepository(connString, providerName));

            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            //    .AddJwtBearer(options => 
            //    { 
            //        options.RequireHttpsMetadata = false; 
            //        options.SaveToken = true; 
            //        options.TokenValidationParameters = new TokenValidationParameters 
            //        { 
            //            ValidateIssuer = true, 
            //            ValidateAudience = true, 
            //            ValidateLifetime = true, 
            //            ValidateIssuerSigningKey = true, 
            //            ValidIssuer = Configuration["JWT: Issuer"], 
            //            ValidAudience = Configuration["Jwt: Audience"], 
            //            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt: SecretKey"])), 
            //            ClockSkew = TimeSpan.Zero 
            //        }; 
            //    });

        }





        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("CorsPolicy");

            //app.UseAuthentication();
            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<EntryHub>("/entryhub");
            });
        }
    }
}
