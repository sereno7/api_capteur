﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using api_heartreate.Models;
using Microsoft.AspNetCore.SignalR;

namespace capteur.Hubs
{
    public class EntryHub : Hub
    {
        public async Task SendEntry(EntryDTO m)
        {
            await Clients.All.SendAsync("NewEntry", m);
        }

        //public async Task ReceptEntry(string m)
        //{
        //    await Clients.All.SendAsync("GetEntry", int.Parse(m));
        //}
    }
}
