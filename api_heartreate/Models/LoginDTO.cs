﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_heartreate.Models
{
    public class LoginDTO
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
