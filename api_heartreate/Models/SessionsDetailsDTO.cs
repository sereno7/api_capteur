﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_heartreate.Models
{
    public class SessionsDetailsDTO
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public int IdUser { get; set; }
        public IEnumerable<EntryDTO> EntryList { get; set; }
    }
}
