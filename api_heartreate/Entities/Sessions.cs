﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_heartreate.Entities
{
    public class Sessions
    {
        public int Id { get; set; }
        public DateTime? Time { get; set; }
        public int IdUser { get; set; }
    }
}