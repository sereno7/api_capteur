﻿using api_heartreate.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace api_heartreate.Repository
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
    }
}
