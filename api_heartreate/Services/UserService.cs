﻿using api_heartreate.Entities;
using api_heartreate.Models;
using api_heartreate.Repository;
using BuildYourTeam.ASP.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_heartreate.Services
{
    public class UserService
    {
        private UserRepository repo;
        private SessionsRepository repo2;

        public UserService(UserRepository repo, SessionsRepository repo2)
        {
            this.repo = repo;
            this.repo2 = repo2;
        }

        public IEnumerable<UserDTO> Get()
        {
            IEnumerable<User> l = repo.Get();
            return l.Select(d =>
            {
                UserDTO e = d.Mapto<UserDTO>();
                return e;
            });
        }


        public IEnumerable<UserDetailsDTO> GetUserDetails()
        {
            IEnumerable<User> l = repo.Get();
            return l.Select(d =>
            {
                UserDetailsDTO r = d.Mapto<UserDetailsDTO>();
                r.SessionsList = repo2.Get().Where((x) => x.IdUser == r.Id)
                    .Select(x => x.Mapto<SessionsDTO>());
                return r;
            });
        }

        public UserDTO GetUserById(int Id)
        {
            UserDTO l = repo.GetById(Id).Mapto<UserDTO>();
            return l;
    
        }

        public UserDetailsDTO GetUserDetailsById(int Id)
        {
            UserDetailsDTO u = repo.Get().FirstOrDefault(x => x.Id == Id).Mapto<UserDetailsDTO>();
            u.SessionsList = repo2.Get().Where((x) => x.IdUser == u.Id)
                .Select(x => x.Mapto<SessionsDTO>());
            return u;
        }

        public int Add(UserDTO e)
        {
            return repo.Insert(e.Mapto<User>());
        }
    }
}
