﻿using api_heartreate.Entities;
using api_heartreate.Models;
using api_heartreate.Repository;
using BuildYourTeam.ASP.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_heartreate.Services
{
    public class SessionsServices
    {
        private SessionsRepository repo;
        private EntryRepository repo2;

        public SessionsServices(SessionsRepository repo, EntryRepository repo2)
        {
            this.repo = repo;
            this.repo2 = repo2;
        }

        public IEnumerable<SessionsDTO> Get()
        {
            IEnumerable<Sessions> l = repo.Get();
            return l.Select(d =>
            {
                SessionsDTO e = d.Mapto<SessionsDTO>();
                return e;
            });
        }
        public int Add(SessionsDTO e)
        {
            e.Time = DateTime.Now;
            return repo.Insert(e.Mapto<Sessions>());
        }

        public IEnumerable<SessionsDetailsDTO> GetSessionsDetails()
        {
            IEnumerable<Sessions> l = repo.Get();
            return l.Select(d =>
            {
                SessionsDetailsDTO r = d.Mapto<SessionsDetailsDTO>();
                r.EntryList = repo2.Get().Where((x) => x.IdSession == r.Id)
                    .Select(x => x.Mapto<EntryDTO>());
                return r;
            });
        }

        public SessionsDTO GetSessionsById(int Id)
        {
            SessionsDTO l = repo.Get().FirstOrDefault(x => x.Id == Id).Mapto<SessionsDTO>();
            return l;

        }

        public SessionsDetailsDTO GetSessionsDetailsById(int Id)
        {
            SessionsDetailsDTO u = repo.Get().FirstOrDefault(x => x.IdUser == Id).Mapto<SessionsDetailsDTO>();
            u.EntryList = repo2.Get().Where((x) => x.Id == u.Id)
                .Select(x => x.Mapto<EntryDTO>());
            return u;
        }

        public IEnumerable<SessionsDetailsDTO> GetSessionsDetailsByUserId(int Id)
        {
            IEnumerable<Sessions> l = repo.Get().Where((x) => x.IdUser == Id);
            return l.Select(d =>
            {
                SessionsDetailsDTO r = d.Mapto<SessionsDetailsDTO>();
                r.EntryList = repo2.Get().Where((x) => x.IdSession == r.Id)
                    .Select(x => x.Mapto<EntryDTO>());
                return r;
            });
        }

        public void DeleteSessionsById(int id)
        {
            repo.Delete(id);
        }

    }
}
