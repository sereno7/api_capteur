﻿using api_heartreate.Entities;
using api_heartreate.Models;
using api_heartreate.Repository;
using BuildYourTeam.ASP.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_heartreate.Services
{
    public class EntryService
    {
        private EntryRepository repo;

        public EntryService(EntryRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<EntryDTO> Get()
        {
            IEnumerable<Entry> l = repo.Get();
            return l.Select(d =>
            {
                EntryDTO e = d.Mapto<EntryDTO>();
                return e;
            });
        }

        public void Add(EntryDTO e)
        {
            repo.Insert(e.Mapto<Entry>());
        }

        public void AddList(IEnumerable<EntryDTO> e)
        {
            foreach (EntryDTO item in e)
            {
                if (item.HeartRate != 0)
                    repo.Insert(item.Mapto<Entry>());
            }
        }
    }
}
