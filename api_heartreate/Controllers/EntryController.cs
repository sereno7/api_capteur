﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_heartreate.Models;
using api_heartreate.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace api_heartreate.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EntryController : ControllerBase
    {
        private EntryService _service;

        public EntryController(EntryService service)
        {
            this._service = service;
        }

        [HttpGet]
        public IEnumerable<EntryDTO> Get()
        {
            return _service.Get().OrderByDescending(x => x.Time).Take(7);
        }

        [HttpPost]
        public void Post(IEnumerable<EntryDTO> e)
        {
           _service.AddList(e);
        }

    }
}