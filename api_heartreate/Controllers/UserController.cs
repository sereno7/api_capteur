﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_heartreate.Models;
using api_heartreate.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace api_heartreate.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserService _service;

        public UserController(UserService service)
        {
            this._service = service;
        }

        [HttpGet]
        public IEnumerable<UserDTO> Get()
        {
            return _service.Get();
        }

        [HttpGet("byid")]
        public UserDTO GetUserById(int id)
        {
            return _service.GetUserById(id);
        }

        [HttpGet("details")]
        public IEnumerable<UserDetailsDTO> GetUserDetails()
        {
            return _service.GetUserDetails();
        }

        [HttpGet("detailsbyid")]
        public UserDetailsDTO GetSessionsDetailsById(int id)
        {
            return _service.GetUserDetailsById(id);
        }

        [HttpPost]
        public int Post(UserDTO e)
        {
           return _service.Add(e);
            
        }
    }
}