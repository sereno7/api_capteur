﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_heartreate.Models;
using api_heartreate.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace api_heartreate.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionsController : ControllerBase
    {
        private SessionsServices _service;

        public SessionsController(SessionsServices service)
        {
            this._service = service;
        }

        [HttpGet]
        public IEnumerable<SessionsDTO> Get()
        {
            return _service.Get();
        }

        [HttpPost]
        public int Post(SessionsDTO e)
        {
           return _service.Add(e);
        }

        [HttpGet("details")]
        public IEnumerable<SessionsDetailsDTO> GetSessionsDetails()
        {
            return _service.GetSessionsDetails();
        }

        [HttpGet("detailsbyid")]
        public SessionsDetailsDTO GetSessionsDetailsById(int id)
        {
            return _service.GetSessionsDetailsById(id);
        }

        [HttpGet("detailsbyuserid/{id}")]
        public IEnumerable<SessionsDetailsDTO> GetSessionsDetailsByUserId(int id)
        {
            return _service.GetSessionsDetailsByUserId(id);
        }

        [HttpDelete("delete/{id}")]
        public void DeleteSessionsById(int id)
        {
            _service.DeleteSessionsById(id);
        }
    }
}